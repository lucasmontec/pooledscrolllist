﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Framework.UI.Pooling
{
    public class GameObjectPool<TBehaviour> where TBehaviour : Behaviour
    {
        private readonly Queue<TBehaviour> _objects = new Queue<TBehaviour>();
        private readonly Func<TBehaviour> _objectFactory;

        private readonly GameObject _poolObjectHolder;
        
        public GameObjectPool(Func<TBehaviour> objectFactory)
        {
            _objectFactory = objectFactory;
            _poolObjectHolder = new GameObject($"ObjectPool<{typeof(TBehaviour).Name}>");
        }

        public TBehaviour Get()
        {
            if (!_objects.Any())
            {
                _objects.Enqueue(_objectFactory());
            }
            
            TBehaviour obj = _objects.Dequeue();
            obj.gameObject.SetActive(true);
            return obj;
        }

        public void Return(TBehaviour obj)
        {
            obj.transform.name = "pooled object";
            obj.transform.SetParent(_poolObjectHolder.transform, false);
            obj.gameObject.SetActive(false);
            _objects.Enqueue(obj);
        }
    }
}