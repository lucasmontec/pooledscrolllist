using UnityEngine;

namespace Framework.UI.PooledScrollList
{
    public abstract class HorizontalPooledScrollList<TItemView> : PooledScrollList<TItemView>
        where TItemView : MonoBehaviour
    {
        public bool startLeft;
        
        protected override float ScrollRectNormalizedPositionOnScrollDirection => 
            Mathf.Clamp01(scrollRect.horizontalNormalizedPosition);

        protected override float CalculateElementSize() => 
            itemViewPrefab.GetComponent<RectTransform>().rect.width;

        protected override float ContentSizeOnScrollDirection => scrollRect.content.sizeDelta.x;

        protected override float ViewportSizeOnDirection => scrollRect.viewport.rect.size.x;

        protected override void SetScrollNormalizedPosition(float normalizedPosition)
        {
            Vector2 position = scrollRect.normalizedPosition;
            position.x = Mathf.Clamp01(normalizedPosition);
            scrollRect.normalizedPosition = position;
        }
        
        protected override void SetSpacerElementSize(float size)
        {
            Spacer.ignoreLayout = size <= 0;
            Spacer.minWidth = size;
            Vector2 sizeDelta = SpacerRect.sizeDelta;
            sizeDelta.x = size;
            SpacerRect.sizeDelta = sizeDelta;
            Spacer.transform.SetSiblingIndex(0);
        }
        
        protected override void SetContentSize(float size)
        {
            Vector2 currentSize = scrollRect.content.sizeDelta;
            currentSize.x = size;
            scrollRect.content.sizeDelta = currentSize;
        }
        
        protected override void ResetPosition()
        {
            scrollRect.horizontalNormalizedPosition = 0f;
        }

        protected override int CalculateVisibleItems()
        {
            float scrollAreaWidth = scrollRect.viewport.rect.width;
            int visibleItems = Mathf.CeilToInt(scrollAreaWidth / ElementSize);
            return visibleItems;
        }

        protected override void OnInitialize()
        {
            scrollRect.vertical = false;
            scrollRect.horizontal = true;

            SetScrollNormalizedPosition(startLeft ? 0 : 1);
        }
    }
}
