namespace Framework.UI.PooledScrollList
{
    public enum ScrollAlignment
    {
        Start,
        Center,
        End
    }
}