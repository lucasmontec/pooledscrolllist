using UnityEngine;

namespace Framework.UI.PooledScrollList
{
    public abstract class VerticalPooledScrollList<TItemView> : PooledScrollList<TItemView>
        where TItemView : MonoBehaviour
    {
        protected override float ScrollRectNormalizedPositionOnScrollDirection => 
            Mathf.Clamp01(1 - scrollRect.verticalNormalizedPosition);

        protected override float CalculateElementSize() => 
            itemViewPrefab.GetComponent<RectTransform>().rect.height;

        protected override float ContentSizeOnScrollDirection => scrollRect.content.sizeDelta.y;
        protected override float ViewportSizeOnDirection => scrollRect.viewport.rect.size.y;
        
        protected override void SetScrollNormalizedPosition(float normalizedPosition)
        {
            Vector2 position = scrollRect.normalizedPosition;
            position.y = Mathf.Clamp01(normalizedPosition);
            scrollRect.normalizedPosition = position;
        }

        protected override void SetSpacerElementSize(float size)
        {
            Spacer.ignoreLayout = size <= 0;
            Spacer.minHeight = size;
            Vector2 sizeDelta = SpacerRect.sizeDelta;
            sizeDelta.y = size;
            SpacerRect.sizeDelta = sizeDelta;
            Spacer.transform.SetSiblingIndex(0);
        }
        
        protected override void SetContentSize(float size)
        {
            Vector2 currentSize = scrollRect.content.sizeDelta;
            currentSize.y = size;
            scrollRect.content.sizeDelta = currentSize;
        }
        
        protected override void ResetPosition()
        {
            scrollRect.verticalNormalizedPosition = 0f;
        }

        protected override int CalculateVisibleItems()
        {
            float scrollAreaHeight = scrollRect.viewport.rect.height;
            int visibleItems = Mathf.CeilToInt(scrollAreaHeight / ElementSize);
            return visibleItems;
        }
        
        protected override void OnInitialize()
        {
            scrollRect.vertical = true;
            scrollRect.horizontal = false;
        }
    }
}
