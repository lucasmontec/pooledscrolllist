﻿using System.Collections.Generic;
using System.Linq;
using Framework.UI.Pooling;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.UI.PooledScrollList
{
    public abstract class PooledScrollList<TItemView> : MonoBehaviour where TItemView : MonoBehaviour
    {
        private const int FirstSiblingIndex = 1;

        protected abstract int ItemCount { get; }

        private GameObjectPool<TItemView> _itemViewPool;

        public TItemView itemViewPrefab;
        public ScrollRect scrollRect;

        public bool initializeOnStart;

        public HorizontalOrVerticalLayoutGroup contentLayout;

        //-1 for spacer item
        private int LoadedItemViewCount => ScrollTransform.childCount - 1;
        private Transform ScrollTransform => scrollRect.content.transform;
        private int _lastCulledItems;

        protected LayoutElement Spacer;
        protected RectTransform SpacerRect;

        protected abstract float ScrollRectNormalizedPositionOnScrollDirection { get; }

        protected abstract float ContentSizeOnScrollDirection { get; }
        protected abstract float ViewportSizeOnDirection { get; }

        protected float ElementSize { get; private set; }

        protected abstract float CalculateElementSize();

        [PublicAPI]
        protected virtual TItemView CreateViewInstance()
        {
            return Instantiate(itemViewPrefab);
        }

        private void Start()
        {
            if (initializeOnStart)
            {
                Initialize();
            }
        }

        [PublicAPI]
        public void JumpTo(int index, ScrollAlignment alignment = ScrollAlignment.Start)
        {
            float normalizedPosition = NormalizedScrollPositionOfIndex(index, alignment);
            SetScrollNormalizedPosition(normalizedPosition);
        }

        [PublicAPI]
        public float NormalizedScrollPositionOfIndex(int index, ScrollAlignment alignment = ScrollAlignment.Start)
        {
            int clampedIndex = Mathf.Clamp(index, 0, ItemCount - 1);
            double elementSize = ElementSize;
            double spacing = contentLayout.spacing;
            
            double itemPosition = clampedIndex * (elementSize + spacing);
            double contentSize = ContentSizeOnScrollDirection;
            double viewportSize = ViewportSizeOnDirection;

            double scrollableSize = contentSize - viewportSize;

            if (scrollableSize <= 0)
                return 1f;

            double desiredViewportOffset;
            switch (alignment)
            {
                case ScrollAlignment.Center:
                    desiredViewportOffset = (viewportSize / 2) - (elementSize / 2);
                    break;
                case ScrollAlignment.End:
                    desiredViewportOffset = viewportSize - elementSize;
                    break;
                case ScrollAlignment.Start:
                default:
                    desiredViewportOffset = 0;
                    break;
            }

            double scrollOffset = itemPosition - desiredViewportOffset;
            
            scrollOffset = Mathf.Clamp((float)scrollOffset, 0f, (float)scrollableSize);
            
            double normalizedPosition = 1 - (scrollOffset / scrollableSize);
            normalizedPosition = Mathf.Clamp01((float)normalizedPosition);

            return (float)normalizedPosition;
        }

        [PublicAPI]
        public void Initialize()
        {
            ElementSize = CalculateElementSize();
            _itemViewPool = new GameObjectPool<TItemView>(CreateViewInstance);

            CreateSpacerElement();
            ResetPosition();

            scrollRect.onValueChanged.AddListener(UpdateView);
            UpdateView();
            OnInitialize();
        }

        private void OnDestroy()
        {
            scrollRect.onValueChanged.RemoveListener(UpdateView);
        }

        private void CreateSpacerElement()
        {
            var spacer = new GameObject("ScrollSpacer");
            spacer.transform.SetParent(scrollRect.content.transform, false);
            spacer.transform.SetSiblingIndex(0);
            Spacer = spacer.AddComponent<LayoutElement>();
            SpacerRect = spacer.GetComponent<RectTransform>();
        }

        [PublicAPI]
        public void UpdateView()
        {
            UpdateView(scrollRect.normalizedPosition);
        }

        private void UpdateView(Vector2 scrollPosition)
        {
            int visibleItems = CalculateVisibleItems();

            float layoutSpacing = 0;
            if (contentLayout)
            {
                layoutSpacing = contentLayout.spacing;
            }

            float totalSpacingSize = (ItemCount - 1) * contentLayout.spacing;
            SetContentSize((ItemCount * ElementSize) + totalSpacingSize);

            int culledItems = CalculateCulledBeforeItems(visibleItems);
            float culledItemsSpacing = (culledItems - 1) * layoutSpacing;
            SetSpacerElementSize((culledItems * ElementSize) + culledItemsSpacing);

            int requiredLoadedItemCount = Mathf.Min(visibleItems + 1, ItemCount);

            bool itemCountIsInvalid = requiredLoadedItemCount != LoadedItemViewCount;
            bool culledItemsChanged = _lastCulledItems != culledItems;

            if (itemCountIsInvalid)
            {
                RefreshItems(requiredLoadedItemCount, culledItems);
            }
            else if (culledItemsChanged)
            {
                MoveItemViewAndLoadData(culledItems);
            }

            _lastCulledItems = culledItems;
        }

        /// <summary>
        /// Called when the item is going to be disposed for reuse.
        /// </summary>
        /// <param name="view">The item being disposed</param>
        [PublicAPI]
        protected virtual void ResetItemView(TItemView view)
        {
        }

        private void MoveItemViewAndLoadData(int culledItems)
        {
            if (LoadedItemViewCount <= 2)
            {
                return;
            }

            bool newItemsVisible = culledItems > _lastCulledItems;
            if (newItemsVisible)
            {
                while (_lastCulledItems < culledItems)
                {
                    MoveFirstItemToLastPosition(_lastCulledItems);
                    _lastCulledItems++;
                }
            }
            else
            {
                while (_lastCulledItems > culledItems)
                {
                    MoveLastItemToFirstPosition(_lastCulledItems);
                    _lastCulledItems--;
                }
            }
        }

        private void MoveLastItemToFirstPosition(int culledItems)
        {
            Transform lastChild = ScrollTransform.GetChild(ScrollTransform.childCount - 1);
            lastChild.SetSiblingIndex(FirstSiblingIndex);
            int previousItemDataIndex = culledItems - 1;
            var itemView = lastChild.GetComponent<TItemView>();
            ResetItemView(itemView);
            SetupView(itemView, previousItemDataIndex);
        }

        private void MoveFirstItemToLastPosition(int culledItems)
        {
            Transform firstChild = ScrollTransform.GetChild(FirstSiblingIndex);
            firstChild.SetAsLastSibling();
            int nextItemDataIndex = culledItems + LoadedItemViewCount;
            var itemView = firstChild.GetComponent<TItemView>();
            ResetItemView(itemView);
            SetupView(itemView, nextItemDataIndex);
        }

        private void RefreshItems(int requiredLoadedItemCount, int culledItems)
        {
            List<Transform> scrollChildren = ScrollTransform.Cast<Transform>().ToList();

            // ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
            foreach (Transform child in scrollChildren)
            {
                if (child.GetSiblingIndex() == 0)
                {
                    continue;
                }

                var view = child.GetComponent<TItemView>();
                ResetItemView(view);
                _itemViewPool.Return(view);
            }

            for (int i = 0; i < requiredLoadedItemCount && i + culledItems < ItemCount; i++)
            {
                TItemView item = _itemViewPool.Get();
                item.transform.SetParent(ScrollTransform, false);
                int dataIndex = i + culledItems;
                SetupView(item, dataIndex);
            }
        }

        /// <summary>
        /// Before items are the items before the scroll start.
        /// On a vertical list it can be the items culled on the top of the list (if that is the initial direction).
        /// On a horizontal list, the items to the left of the list start (if that is the initial direction).
        /// </summary>
        private int CalculateCulledBeforeItems(int visibleItems)
        {
            float fCulledItems = ScrollRectNormalizedPositionOnScrollDirection * (ItemCount - visibleItems);
            int culledItems = Mathf.FloorToInt(fCulledItems);
            int remainingItems = Mathf.Max(ItemCount - (visibleItems + 1), 0);
            int clampedCulledItems = Mathf.Clamp(culledItems, 0, remainingItems);
            return clampedCulledItems;
        }

        protected abstract void OnInitialize();

        protected abstract void SetScrollNormalizedPosition(float normalizedPosition);

        protected abstract void SetSpacerElementSize(float size);

        protected abstract void SetContentSize(float size);

        protected abstract void SetupView(TItemView view, int dataIndex);

        protected abstract void ResetPosition();

        protected abstract int CalculateVisibleItems();
    }
}