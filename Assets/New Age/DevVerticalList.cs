using Framework.UI.PooledScrollList;

namespace New_Age
{
    public class DevVerticalList : VerticalPooledScrollList<DevTestCell>
    {
        public int itemCount;

        protected override int ItemCount => itemCount;
        
        protected override void SetupView(DevTestCell view, int dataIndex)
        {
            view.SetLabel($"cell {dataIndex}");
        }
    }
}