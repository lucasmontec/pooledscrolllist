using TMPro;
using UnityEngine;

namespace New_Age
{
    public class DevTestCell : MonoBehaviour
    {
        public TextMeshProUGUI label;

        public void SetLabel(string text)
        {
            label.text = text;
        }
    }
}