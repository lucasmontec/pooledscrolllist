using Inventory;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InventoryInfoPanel : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI nameText;
    [SerializeField]
    private TextMeshProUGUI descriptionText;
    [SerializeField]
    private TextMeshProUGUI statsText;

    [SerializeField]
    private Image iconImage;

    [SerializeField]
    private IconRegistry iconRegistry;

    [SerializeField]
    private InventoryItemPooledScrollView pooledScrollView;

    private void OnEnable()
    {
        pooledScrollView.OnClicked += PooledScrollViewOnOnClicked;
    }

    private void OnDisable()
    {
        pooledScrollView.OnClicked -= PooledScrollViewOnOnClicked;
    }

    private void PooledScrollViewOnOnClicked(InventoryItemView item, InventoryItemData data, int _)
    {
        SetData(data);
    }

    private void SetData(InventoryItemData data)
    {
        nameText.text = data.Name;
        descriptionText.text = data.Description;
        statsText.text = $"{data.Stat}";

        iconImage.sprite = iconRegistry[data.IconIndex];
    }
}
