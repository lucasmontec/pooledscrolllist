using System;
using System.Collections.Generic;
using Inventory;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public GameObject Container;

    public InventoryItemPooledScrollView pooledScrollView;
    
    [Tooltip("Loads the list using this format.")]
    [Multiline]
    public string ItemJson;

    [Tooltip("This is used in generating the items list. The number of additional copies to concat the list parsed from ItemJson.")]
    public int ItemGenerateScale = 10;
    
    [Serializable]
    private class InventoryItemDatas
    {
        public InventoryItemData[] ItemDatas;
    }

    private InventoryItemData[] ItemDatas;

    private List<InventoryItemView> Items;
    
    private void Awake()
    {
        RemoveCurrentListComponents();

        ItemDatas = GenerateItemDatas(ItemJson, ItemGenerateScale);
        
        pooledScrollView.OnClicked += InventoryItemOnClick;
        pooledScrollView.SelectItemAction = SelectItemAction;
        pooledScrollView.DeselectItemAction = DeselectItemAction;
        
        pooledScrollView.SetData(ItemDatas);
        pooledScrollView.Initialize();
        pooledScrollView.UpdateView();
    }

    private void OnDestroy()
    {
        pooledScrollView.OnClicked -= InventoryItemOnClick;
        pooledScrollView.SelectItemAction = null;
        pooledScrollView.DeselectItemAction = null;
    }
    
    private void RemoveCurrentListComponents()
    {
        var items = Container.GetComponentsInChildren<InventoryItemView>();
        foreach (InventoryItemView item in items)
        {
            item.gameObject.transform.SetParent(null);
        }
    }

    /// <summary>
    /// Generates an item list.
    /// </summary>
    /// <param name="json">JSON to generate items from. JSON must be an array of InventoryItemData.</param>
    /// <param name="scale">Concats additional copies of the array parsed from json.</param>
    /// <returns>An array of InventoryItemData</returns>
    private InventoryItemData[] GenerateItemDatas(string json, int scale) 
    {
        var itemDatas = JsonUtility.FromJson<InventoryItemDatas>(json).ItemDatas;
        var finalItemDatas = new InventoryItemData[itemDatas.Length * scale];
        for (var i = 0; i < itemDatas.Length; i++) {
            for (var j = 0; j < scale; j++) {
                finalItemDatas[i + j*itemDatas.Length] = itemDatas[i];
            }
        }

        return finalItemDatas;
    }

    private void InventoryItemOnClick(InventoryItemView itemViewClicked, InventoryItemData _, int dataIndex) 
    {
        pooledScrollView.SelectItem(itemViewClicked, dataIndex);
    }
    
    private static void DeselectItemAction(InventoryItemView item)
    {
        item.Background.color = Color.white;
    }
    
    private static void SelectItemAction(InventoryItemView item)
    {
        item.Background.color = Color.red;
    }
}
