﻿using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu(menuName = "Inventory/Icon Registry")]
    public class IconRegistry : ScriptableObject
    {
        [SerializeField]
        private Sprite[] icons;

        public Sprite this[int iconIndex] => icons[iconIndex];
    }
}