﻿using System;
using System.Collections.Generic;
using Framework.UI.PooledScrollList;

namespace Inventory
{
    public class InventoryItemPooledScrollView : VerticalPooledScrollList<InventoryItemView>
    {
        protected override int ItemCount => _data.Count;

        public IconRegistry iconRegistry;

        private List<InventoryItemData> _data;

        public delegate void ItemClickDelegate(InventoryItemView item, InventoryItemData data, int dataIndex);
        public event ItemClickDelegate OnClicked;

        public delegate void ItemSelectDelegate(InventoryItemView item);
        public ItemSelectDelegate SelectItemAction;
        public ItemSelectDelegate DeselectItemAction;

        private int _selectedDataIndex;

        private InventoryItemView _previousView;
        
        public void SelectItem(InventoryItemView item, int dataIndex)
        {
            _selectedDataIndex = dataIndex;
            SelectItemAction?.Invoke(item);
        }

        protected override void ResetItemView(InventoryItemView view)
        {
            DeselectItemAction?.Invoke(view);
        }

        public void SetData(IEnumerable<InventoryItemData> data)
        {
            _data = new List<InventoryItemData>(data);
        }

        protected override void SetupView(InventoryItemView view, int dataIndex)
        {
            if (_data == null)
            {
                throw new Exception("Data was not initialized! Call SetData on awake.");
            }
            
            var itemData = _data[dataIndex];
            view.Icon.sprite = iconRegistry[itemData.IconIndex];
            view.Name.text = itemData.Name;
            view.Button.onClick.AddListener(() =>
            {
                ResetPreviousSelection();
                OnClicked?.Invoke(view, itemData, dataIndex);
                _previousView = view;
            });

            if (dataIndex == _selectedDataIndex)
            {
                SelectItem(view, dataIndex);
                OnClicked?.Invoke(view, itemData, dataIndex);
                _previousView = view;
            }

            view.transform.name = $"#{dataIndex} {itemData.Name}";
        }

        private void ResetPreviousSelection()
        {
            if (!_previousView) return;
            ResetItemView(_previousView);
        }
    }
}