using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InventoryItemView : MonoBehaviour
{
    public Image Background;
    public Image Icon;
    public TextMeshProUGUI Name;
    public Button Button;
}
